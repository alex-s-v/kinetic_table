var is_vertical = true;
var data = [
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
];

var container = document.getElementById('example');
var hot = new Handsontable(container, {
    data: data,
    header: true,
    rowHeaders: true,
    colHeaders: true,
    licenseKey: 'non-commercial-and-evaluation'
});


var btn_trn = document.getElementById("btn_trn");
btn_trn.addEventListener("click", function() {
    data = transpose(data);
    is_vertical = !is_vertical;
    container.innerHTML = "";
    hot = new Handsontable(container, {
        data: data,
        rowHeaders: true,
        colHeaders: true,
        licenseKey: 'non-commercial-and-evaluation'
    });
});


var btn_clr = document.getElementById("btn_clr");
btn_clr.addEventListener("click", function() {
    hot.clear();
    document.getElementById("plot").innerHTML = "";
    document.getElementById("k").innerHTML = "";
});


var btn_prc = document.getElementById("btn_prc");
btn_prc.addEventListener("click", function() {
    process(data);
});


function transpose(array) {
    return array[0].map((col, i) => array.map(row => row[i]));
}

function calculate_exp_params(func, xs, ys, init_guess) {
    var fun = (x, P) => {
        return x.map((xi) => func(xi, P));
    };
    return fminsearch(fun, init_guess, xs, ys, {maxIter:10000});
}

function approximate(xs, ys, alg) {
    var exp_dec_2_order = function (x, P) {
        return ys[0] + P[0]*Math.exp(-P[1]*x) + P[2]*Math.exp(-P[3]*x);
    };
    var exp_dec_1_order = function (x, P) {
        return P[0] + P[1] * Math.exp(P[2]*x);
    };
    var func = null;
    var guess = null;    
    switch (alg) {
        case "second_order":
            func = exp_dec_2_order;
            guess = [-1, -1, 1, 1];
            break;
        case "first_order":
            func = exp_dec_1_order;
            guess = [0, 1, 0];
            break;
        default:
            break;
    }
    prms = calculate_exp_params(func, xs, ys, guess);
    console.log(prms);
    const n_steps = 100;
    const min_x = Math.min(...xs);
    const max_x = Math.max(...xs);
    const step = (max_x - min_x) / n_steps;
    var nnxs = [];
    var nnys = [];
    var x = min_x
    for (var i = 0; i < n_steps; i++) {
        x += step;
        nnxs.push(x);
        nnys.push(func(x, prms));
    }
    return [nnxs, nnys]
}


function process(data) {
    var xs = [];
    var ys = [];
    if (is_vertical) {
        for (var i = 0; i < data.length; i++) {
            if (data[i][0] != null && data[i][0] != null) {
                xs.push(data[i][0]);
                ys.push(data[i][1]);
            }
        }
    } else {
        xs = data[0].filter(x => x != null);
        ys = data[1].filter(x => x != null);
    }
    ys = ys.map(x => parseFloat(x));
    xs = xs.map(x => parseFloat(x));
    var rbtns = document.getElementsByName("app_eq");
    var alg = "";
    rbtns.forEach(rbtn => {
        if (rbtn.checked) {
            alg = rbtn.value;
        }
    });
    var [nnxs, nnys] = approximate(xs, ys, alg);
    var nxs = nnxs.slice(1);
    var nys = nnys.slice(1).map(x => Math.log(nnys[0] / x));
    var nxs2 = xs.slice(1);
    var nys2 = ys.slice(1).map(x => Math.log(nnys[0] / x));
    var res = main_alg(nxs, nys);
    var res2 = main_alg(nxs2, nys2);
    document.getElementById("k_interp").innerHTML = `y = ${res.k}*x + ${res.b}`;
    document.getElementById("k").innerHTML = `y = ${res2.k}*x + ${res2.b}`;

    do_plots(nxs, nys, res.xs, res.ys, xs, ys, nnxs, nnys, nxs2, nys2, res2.xs, res2.ys);
    push_to_table(nxs, nnys.slice(1), nys);
}


function main_alg(xs, ys) {
    var x_avg = sum(xs) / xs.length;
    var y_avg = sum(ys) / ys.length;
    var m1 = 0;
    var m2 = 0;
    for (var i = 0; i < xs.length; i++) {
        m1 += ((xs[i]-x_avg)*(ys[i]-y_avg));
        m2 += Math.pow((xs[i]-x_avg), 2);
    }
    var m = m1 / m2;
    var b = y_avg - m*x_avg;
    var f = x => m*x + b;
    return {
        xs: [xs[0], xs[xs.length-1]],
        ys: [f(xs[0]), f(xs[xs.length-1])],
        k: m,
        b: b
    };
}


function push_to_table(xs, ys, zs) {
    var container = document.getElementById('result_table');
    container.innerHTML = "";
    var data = [];
    rn = 1000; // round number
    for (var i = 0; i < xs.length; i++) {
        data.push([Math.round(xs[i] * rn) / rn, ys[i], zs[i]]);
    }
    var h = new Handsontable(container, {
        data: data,
        header: true,
        rowHeaders: true,
        colHeaders: true,
        licenseKey: 'non-commercial-and-evaluation'
    });
}


function do_plots(exp_xs, exp_ys, calc_xs, calc_ys, xs, ys, nxs, nys, nxs2, nys2, calc_xs2, calc_ys2) {
// function do_plots(exp_xs, exp_ys, calc_xs, calc_ys) {
    var config = {
        toImageButtonOptions: {
          format: 'png',
          filename: 'plot'
        }
    };
    var data1 = {
        x: calc_xs,
        y: calc_ys,
        mode: "lines",
        name: "Approximation on interp"
    };
    var data2 = {
        x: exp_xs,
        y: exp_ys,
        mode: "markers",
        name: "Experiment interp"
    };
    var data12 = {
        x: calc_xs2,
        y: calc_ys2,
        mode: "lines",
        name: "Approximation"
    };
    var data22 = {
        x: nxs2,
        y: nys2,
        mode: "markers",
        name: "Experiment"
    };
    var data3 = {
        x: xs,
        y: ys,
        xaxis: "x2",
        yaxis: "y2",
        mode: "markers",
        name: "Experiment"
    };
    var data4 = {
        x: nxs,
        y: nys,
        xaxis: "x2",
        yaxis: "y2",
        mode: "lines",
        name: "Approximation"
    };
    var layout = {
        title: "t = f( ln(D0 / D) )",
        xaxis: {title: "Time [min]"},
        yaxis: {title: "ln(D0 / D)"},
        xaxis2: {title: "Time [min]"},
        yaxis2: {title: "D"},
        grid: {rows: 1, columns: 2, pattern: 'independent'}
    };
    Plotly.newPlot('plot', [data1, data2, data12, data22, data3, data4], layout, config);
    // Plotly.newPlot('plot', [data1, data2], layout, config);
}

function sum(array) {
    var s = 0;
    for (var i = 0; i < array.length; i++) {
        s += array[i];
    }
    return s;
}
